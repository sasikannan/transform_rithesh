{{ config(materialized='table') }}

with final as(
select sbqq__account__c as account,
       account.name as account_name,
       sbqq__productname__c as product_name,
       sbqq__productid__c as product_id,
       case
        when sbqq__billingfrequency__c = 'Annual' then sbqq__netprice__c / 12
        when sbqq__billingfrequency__c = 'Quarterly' then sbqq__netprice__c / 4
        when sbqq__billingfrequency__c = 'Semiannual' then sbqq__netprice__c / 6
        else sbqq__netprice__c
       end as net_price,
       sbqq__quantity__c as quantity,
       case
        when sbqq__subscriptionstartdate__c__t is null then sbqq__startdate__c__t
        else sbqq__subscriptionstartdate__c__t
       end as start_date,
       case
        when sbqq__subscriptionenddate__c__t is null then sbqq__enddate__c__t
        else sbqq__subscriptionenddate__c__t
       end as end_date
    from sbqq__subscription__c as subscriptions left join account on subscriptions.sbqq__account__c = account.id)

select * from final

